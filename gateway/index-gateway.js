const express = require('express');

const http = require('http');

const app = express();
//creamos un server de http habilitado con espress
const server = http.createServer(app);

const { Server } = require('socket.io');
//creamos una nueva constaste para instaciar el servidor http
const io = new Server(server);
//solicitamos el servicio de la api con bull - Libros - Socios - Pagos
const apiSocios = require('api-socios');

const apiLibros = require('api-libros');

const apiPagos = require('api-pagos');

const port = 80;
//inicializamos el server
server.listen(port, () => {
    console.log("servidor inicializado", port);

    io.on('connection', (socket) => {

        console.log("new connection", socket.id);

        /**SOCIOS */

        socket.on('req:socios:view', async () => {

            try {

                console.log('req:socios:view');

                const { statusCode, data, message } = await apiSocios.View();
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:socios:view', { statusCode, data, message });

            } catch (error) {
                console.log(error)

            }

        });

        socket.on('req:socios:create', async ({ name, phone }) => {

            try {

                console.log('req:socios:create', ({ name, phone }));

                const { statusCode, data, message } = await apiSocios.Create({ name, phone });

                io.to(socket.id).emit('res:socios:create', { statusCode, data, message });

                const view = await apiSocios.View();
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:socios:view', view);


            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:socios:delete', async ({ id }) => {

            try {

                console.log('req:socios:delete', ({ id }));

                const { statusCode, data, message } = await apiSocios.Delete({ id });

                io.to(socket.id).emit('res:socios:delete', { statusCode, data, message });

                const view = await apiSocios.View();
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:socios:view', view);

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:socios:update', async ({ email, phone, age, id, name }) => {

            try {

                console.log('req:socios:update', ({ email, phone, age, id, name }));

                const { statusCode, data, message } = await apiSocios.Update({ email, phone, age, id, name });

                return io.to(socket.id).emit('res:socios:update', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:socios:findOne', async ({ id }) => {

            try {

                console.log('req:socios:findOne', ({ id }));

                const { statusCode, data, message } = await apiSocios.FindOne({ id });

                return io.to(socket.id).emit('res:socios:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
        });
        // ENABLE-DISABLE SOCIOS
        socket.on('req:socios:enable', async ({ id }) => {

            try {

                console.log('req:socios:enable', ({ id }));

                const { statusCode, data, message } = await apiSocios.Enable({ id });

                io.to(socket.id).emit('res:socios:enable', { statusCode, data, message });

                const view = await apiSocios.View();
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:socios:view', view);


            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:socios:disable', async ({ id }) => {

            try {

                console.log('req:socios:disable', ({ id }));

                const { statusCode, data, message } = await apiSocios.Disable({ id });

                io.to(socket.id).emit('res:socios:disable', { statusCode, data, message });

                const view = await apiSocios.View();
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:socios:view', view);


            } catch (error) {
                console.log(error);
            }
        });

        /* LIBROS */
        socket.on('req:libros:view', async ({ }) => {

            try {

                console.log('req:libros:view');

                const { statusCode, data, message } = await apiLibros.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:libros:view', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }

        });

        socket.on('req:libros:create', async ({ title, image }) => {

            try {

                console.log('req:libros:create', ({ title, image }));

                const { statusCode, data, message } = await apiLibros.Create({ title, image });

                io.to(socket.id).emit('res:libros:create', { statusCode, data, message });

                const view = await apiLibros.View({});

                return io.to(socket.id).emit('res:libros:view', view);

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:libros:delete', async ({ id }) => {

            try {

                console.log('req:libros:delete', ({ id }));

                const { statusCode, data, message } = await apiLibros.Delete({ id })

                io.to(socket.id).emit('res:libros:delete', { statusCode, data, message })

                const view = await apiLibros.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:libros:view', view);

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:libros:update', async ({ title, image, category, seccions, id }) => {

            try {

                console.log('req:libros:update', ({ title, image, category, seccions, id }));

                const { statusCode, data, message } = await apiLibros.Update({ title, image, category, seccions, id });

                return io.to(socket.id).emit('res:libros:update', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:libros:findOne', async ({ title }) => {

            try {

                console.log('req:libros:findOne', ({ title }));

                const { statusCode, data, message } = await apiLibros.FindOne({ title });

                return io.to(socket.id).emit('res:libros:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
        });

        /* PAGOS */
        socket.on('req:pagos:view', async ({ }) => {

            try {

                console.log('req:pagos:view');

                const { statusCode, data, message } = await apiPagos.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:pagos:view', { statusCode, data, message });

            } catch (error) {
                console.log(error)

            }

        });

        socket.on('req:pagos:create', async ({ socio, amount }) => {

            try {

                console.log('req:pagos:create', ({ socio, amount }));

                const { statusCode, data, message } = await apiPagos.Create({ socio, amount });

                io.to(socket.id).emit('res:pagos:create', { statusCode, data, message });

                const view = await apiPagos.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:pagos:view', view );

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:pagos:delete', async ({ id }) => {

            try {

                console.log('req:pagos:delete', ({ id }));

                const { statusCode, data, message } = await apiPagos.Delete({ id });

                io.to(socket.id).emit('res:pagos:delete', { statusCode, data, message });

                const view = await apiPagos.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:pagos:view', view );

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:pagos:findOne', async ({ id }) => {

            try {

                console.log('req:pagos:findOne', ({ id }));

                const { statusCode, data, message } = await apiPagos.FindOne({ id });

                return io.to(socket.id).emit('res:pagos:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
        });
    });
});


/* CONEXIONES MANUALES 

const apiSocios = require('../socios/api/test');

const apiLibros = require('../libros/api/test');

const apiPagos = requi('../pagos/api/test');


            socket.on('req:microservice:view', async({ }) => {

            try {

                console.log('req:microservice:view');
                
                const { statusCode, data, message } = await apiSocios.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:microservice:view', { statusCode, data, message });
                
            } catch (error) {
                console.log(error)
                
            }
            
        });

        socket.on('req:microservice:create', async({ name,  phone }) => {

           try {
            
                console.log('req:microservice:create', ({ name,  phone }));

                const { statusCode, data, message } = await apiSocios.Create({ name,  phone });

                return io.to(socket.id).emit('res:microservice:create', { statusCode, data, message });

           } catch (error) {
                console.log(error)
           }
        });

        socket.on('req:microservice:delete', async({ id }) => {

            try {

                console.log('req:microservice:delete', ({ id }));

                const { statusCode, data, message } = await apiSocios.Delete({ id });

                return io.to(socket.id).emit('res:microservice:delete', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:microservice:update', async({ email, phone, age, id, name }) => {

            try {

                console.log('req:microservice:update', ({ email, phone, age, id, name }));

                const { statusCode, data, message } = await apiSocios.Update({ email, phone, age, id, name });

                return io.to(socket.id).emit('res:microservice:update', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:microservice:findOne', async({ id }) => {
            
            try {

                console.log('req:microservice:findOne', ({ id }));

                const { statusCode, data, message } = await apiSocios.FindOne({ id });

                return io.to(socket.id).emit('res:microservice:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
        });
        // ENABLE-DISABLE SOCIOS
        socket.on('req:microservice:enable', async({ id }) => {
            
            try {

                console.log('req:microservice:enable', ({ id }));

                const { statusCode, data, message } = await apiSocios.Enable({ id });

                return io.to(socket.id).emit('res:microservice:enable', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
        });

        socket.on('req:microservice:disable', async({ id }) => {
            
            try {

                console.log('req:microservice:disable', ({ id }));

                const { statusCode, data, message } = await apiSocios.Disable({ id });

                return io.to(socket.id).emit('res:microservice:disable', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
        });
    
        /* LIBROS 
        socket.on('req:microservice:view', async({ }) => {

            try {

                console.log('req:microservice:view');
                
                const { statusCode, data, message } = await apiLibros.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:microservice:view', { statusCode, data, message });
                
            } catch (error) {
                console.log(error)  
            }
            
        });

        socket.on('req:microservice:create', async({ title }) => {

           try {
            
                console.log('req:microservice:create', ({ title }));

                const { statusCode, data, message } = await apiLibros.Create({ title });

                return io.to(socket.id).emit('res:microservice:create', { statusCode, data, message });

           } catch (error) {
                console.log(error)
           }
        });

        socket.on('req:microservice:delete', async({ id }) => {

            try {

                console.log('req:microservice:delete', ({ id }));

                const { statusCode, data, message } = await apiLibros.Delete({ id });

                return io.to(socket.id).emit('res:microservice:delete', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:microservice:update', async({ title, category, seccions, id }) => {

            try {

                console.log('req:microservice:update', ({ title, category, seccions, id}));

                const { statusCode, data, message } = await apiLibros.Update({ title, category, seccions, id });

                return io.to(socket.id).emit('res:microservice:update', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:microservice:findOne', async({ title }) => {
            
            try {

                console.log('req:microservice:findOne', ({ title }));

                const { statusCode, data, message } = await apiLibros.FindOne({ title });

                return io.to(socket.id).emit('res:microservice:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
        });

        /* PAGOS 
        socket.on('req:microservice:view', async({ }) => {

            try {

                console.log('req:microservice:view');
                
                const { statusCode, data, message } = await apiPagos.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:microservice:view', { statusCode, data, message });
                
            } catch (error) {
                console.log(error)
                
            }
            
        });

        socket.on('req:microservice:create', async({ socio, amount }) => {

           try {
            
                console.log('req:microservice:create', ({ socio, amount }));

                const { statusCode, data, message } = await apiPagos.Create({ socio, amount });

                return io.to(socket.id).emit('res:microservice:create', { statusCode, data, message });

           } catch (error) {
                console.log(error)
           }
        });

        socket.on('req:microservice:delete', async({ id }) => {

            try {

                console.log('req:microservice:delete', ({ id }));

                const { statusCode, data, message } = await apiPagos.Delete({ id });

                return io.to(socket.id).emit('res:microservice:delete', { statusCode, data, message });

            } catch (error) {
                console.log(error)
            }
        });

        socket.on('req:microservice:findOne', async({ id }) => {
            
            try {

                console.log('req:microservice:findOne', ({ id }));

                const { statusCode, data, message } = await apiPagos.FindOne({ id });

                return io.to(socket.id).emit('res:microservice:findOne', { statusCode, data, message });

            } catch (error) {
                console.log(error);
            }
        });

*/