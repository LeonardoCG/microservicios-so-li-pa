const { Model } = require('../Models/db');

//metodos básicos
const Create = async({ socio, amount }) => {
   try {

    let instance = await Model.create(
            { socio, amount }, 
            { fields: ['socio', 'amount'], logging: false }
        );
        //obtener los datos con .toJSON
        return { statusCode: 200, data: instance.toJSON() }

   } catch (error) {
        console.log({step: 'Controller Create', error: error.toString()});

        return { statusCode: 500, message: error.toString()}
   }
}

const Delete = ({ where = {} }) => {
    try {

        Model.destroy({ where, logging: false });
        //obtener los datos con .toJSON
        return { statusCode: 200, data: "ok" } 
 
    } catch (error) {
         console.log({step: 'Controller Delete', error: error.toString()});
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

const FindOne = async({ where = {} }) => {
    try {
     
        let instance = await Model.findOne({ where, logging: false });
        //obtener los datos con .toJSON 
        if( instance ) return { statusCode: 200, data: instance.toJSON() }

        else return {statusCode: 400, message: "No existe el pago de socio"}
 
    } catch (error) {
         console.log({step: 'Controller FindOne', error: error.toString()});
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

const View = async({ where = {} }) => {
    try {

        let instances = await Model.findAll({ where , logging: false });
        //obtener los datos con .toJSON
        return { statusCode: 200, data: instances }

    } catch (error) {
         console.log({step: 'Controller View', error: error.toString()});
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

module.exports = {
    Create, Delete, FindOne, View
}