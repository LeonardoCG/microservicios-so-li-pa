const Controllers = require('../Controllers/controllers');
const { internalError, redis } = require('../settings');
const Socios = require('api-socios');

//Servicios  || exterior
const Create = async({ socio, amount }) => {

    try {

        const validSocio = await Socios.FindOne({ id: socio }, redis);

        if( validSocio.statusCode !== 200) {

            switch (validSocio.statusCode) {
                case 400: {

                    return { statusCode: 400, message: "No existe el usuario" }
                }  
                default: return { statusCode: 400, message: internalError }
            }
        }

        if (!validSocio.data.enable) {
            return { statusCode: 400, message: "usuario no activo" }
        }

        let { statusCode, data, message } = await Controllers.Create({ socio, amount });

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service Create', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const Delete = async({ id }) => {

    try {

        const findOne = await Controllers.FindOne({ where: { id }});

        if( findOne.statusCode !== 200 ) {

            switch( findOne.statusCode ) {

                case 400: return { statusCode: 400, message: "No existe el pago del socio eliminado" }

                default:  return { statusCode: 500, message: internalError }
            }
        }

        const del = await Controllers.Delete({ where: { id } });

        if( del.statusCode === 200 ) return { statusCode: 200, data: findOne.data }

        return { statusCode: 400, message: internalError }

    } catch (error) {
       
        console.log({ step: 'service Delete', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const FindOne = async({ id }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.FindOne({ where: { id }});

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service FindOne', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const View = async({ }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.View({ where: { }});

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service View', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

module.exports = {
    Create,
    Delete,
    FindOne,
    View
}
