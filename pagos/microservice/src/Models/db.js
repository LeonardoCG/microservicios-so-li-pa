const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');


//modelo 
const Model = sequelize.define('pago', {

    socio: { type: DataTypes.BIGINT },

    amount: { type: DataTypes.BIGINT },

});

//iniciarlizar el model 
const syncroDB = async() => {

    try {
        
        // detnemos el registro de logueo { logging: false }
        await Model.sync({ logging: false })

        console.log('base de datos iniciada - Pagos ')

        return { statusCode: 200, data: 'ok'}
 
    } catch (error) {
         console.log( error );
 
         return { statusCode: 500, message: error.toString()}
 
    }
}


module.exports = { Model, syncroDB };