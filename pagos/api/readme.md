## API DE PAGOS

esta es una api para interactuar con los PAGOS registrados 

'''js
    const apiPagos = require('api-pagos');

    socket.on('req:pagos:view', async({ }) => {

            try {

                console.log('req:pagos:view');
                
                const { statusCode, data, message } = await apiPagos.View({});
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:pagos:view', { statusCode, data, message });
                
            } catch (error) {
                console.log(error)
                
            }
            
        });