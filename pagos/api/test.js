// libreria npm i bull
const bull = require('bull');

const { name } = require('./package.json')

const redis = { host: '192.168.1.105' , port: 6379 }

const opts = { redis: { host: redis.host, port: redis.port } }

const queueCreate = bull(`${name.replace('api-', '')}:create`, opts);

const queueDelete = bull(`${name.replace('api-', '')}:delete`, opts);

const queueFindOne = bull(`${name.replace('api-', '')}:findOne`, opts);

const queueView = bull(`${name.replace('api-', '')}:view`, opts);


const Create = async({ socio, amount }) => {

    try {
        const job = await queueCreate.add({ socio, amount });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const Delete = async({ id }) => {

    try {
        const job = await queueDelete.add({ id });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }
        

    } catch (error) {
        console.log(error)
    }

}

const FindOne = async({ id }) => {

    try {
        const job = await queueFindOne.add({ id });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const View = async({ }) => {

    try {

        const job = await queueView.add({});

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

module.exports =  { Create, Delete, FindOne, View }