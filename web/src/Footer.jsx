import React, {useEffect, useState} from "react";
import socket from "socket.io-client";

const Footer = () => {
  const [id, setId] = useState();
  //renderizamos cada 500 milisegundos
  useEffect(() => {
    setTimeout(() => setId(socket.id), 500);
  }, []);

  return (
    <div>
      <p>{id ? `Estas en linea ${id}` : "Fuera de linea"}</p>
    </div>
  );
};

export default Footer;
