import React, { useEffect, useState } from "react";
import { socket } from "./ws";
import styled from "styled-components";

const Container = styled.div`
  position: relative;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: space-evenly;
  align-self: center;
  display: flex;
  width: 100%;
`;
const ContainerLibro = styled.div`
  background-color: #303530;
  margin-bottom: 15px;
  border-radius: 10px;
  padding: 2px;
  position: relative;
`;
const Portada = styled.img`
  width: 200px;
`;
const Icon = styled.div`
  width: 35px;
  height: 35px;
  position: absolute;
  color: red;
  top: -14px;
  right: -14px;
  cursor: pointer;
`;
const Title = styled.p`
  color: white;
  text-align: center;
`;
const Button = styled.button`
  background-color: #039e00;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-radius: 15px;
  opacity: 0.8;
  color: white;
  width: 100%;
  height: 50px;
  margin-bottom: 25px;

  cursor: pointer;
`;
const Category = styled.p``;
const Seccions = styled.p``;

const Item = ({ image, title, id }) => {
  const handleDelete = () => socket.emit("req:libros:delete", { id });

  return (
    <ContainerLibro>
      <Icon onClick={handleDelete}>
        <svg
          xmlns="http://www.w3.org/2000/svg"
          className="h-5 w-5"
          viewBox="0 0 20 20"
          fill="currentColor"
        >
          <path
            fillRule="evenodd"
            d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
            clipRule="evenodd"
          />
        </svg>
      </Icon>
      <Portada src={image} />
      <Title>{title}</Title>
    </ContainerLibro>
  );
};

const Libros = () => {
  //definimos para setear
  const [data, setData] = useState([]);

  useEffect(() => {
    //socket que escucha
    socket.on("res:libros:view", ({ statusCode, data, message }) => {
      console.log("res:libros:view", { statusCode, data, message });
      //recibiendo datos
      console.log({ statusCode, data, message });

      if (statusCode === 200) {
        setData(data);
      }
    });
    setTimeout(() => socket.emit("req:libros:view", {}), 1000);
  }, []);

  const handleCreate = () => socket.emit('req:libros:create', {
    image: "https://nodejs.org/static/images/logo.svg", 
    title: "Aprende nodejs"   
  });

  return (
    <Container>
      <Button onClick={handleCreate}>Create Libro</Button>
      {data.map((v, i) => (
        <Item key={i} {...v} />
      ))}
    </Container>
  );
};

export default Libros;
