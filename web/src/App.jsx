import React from "react";
import styled from "styled-components";

import Footer from "./Footer";
import Libros from "./Libros";
import Socios from "./Socios";
import Pagos from "./Pagos";

const ContanerApp = styled.div`
  display: flex;
  flex-direction: row;
  min-height: calc(100vh);
  background: #ffffff;
`;
const SliderApp = styled.div`
  width: 35%;
  min-width: 350px;
  margin: 5px;
`;
const BodyApp = styled.div`
  width: 65%;
  min-width: 350px;
  margin: 5px;
`;

function App() {
  return (
    <ContanerApp>
      <SliderApp>
        <Socios />
        <Pagos />
      </SliderApp>

      <BodyApp>
        <Libros />
        <Footer />
      </BodyApp>

      
    </ContanerApp>
  );
}

export default App;
