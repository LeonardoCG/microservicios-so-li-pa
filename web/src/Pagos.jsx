import React, { useEffect, useState } from "react";
import { socket } from "./ws";
import styled from "styled-components";

const Container = styled.div`
  width: 100%;
  max-width: 700px;
  height: 350px;
  margin-bottom: 15px;
  overflow-y: scroll;
`;
const ContainerSocio = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 10px;
  background-color: #303530;
  border-radius: 20px;
`;
const Body = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  height: 100px;
  text-align: left;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  position: relative;
`;
const Button = styled.button`
  background-color: #039e00;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-radius: 15px;
  opacity: 0.8;
  color: white;
  width: 100%;
  height: 50px;
  margin-bottom: 25px;

  cursor: pointer;
`;
const Name = styled.p`
  /* color: #333; */
  color: white;
`;
const Icon = styled.div`
  width: 35px;
  height: 35px;
  color: red;
  opacity: 0.8;
  cursor: pointer;
`;
const Input = styled.input`
  border-radius: 5px;
  width: 90%;
  text-align: center;
  border-color: blue;
  justify-content: center;
  height: 30px;
  margin-left: 10px;
  margin-bottom: 20px;
  cursor: pointer;
`;
const I = styled.div`
  position: absolute;
  width: 80px;
  height: 25px;
  top: 8px;
  border-radius: 60%;
  background-color: #b6b6be;
  right: 8px;
  text-align: center;
  padding-top: 5px;
  color: #010101;
  opacity: 0.8;
`;
const Feedback = styled.div`
  margin: 10px;
  align-items: center;
  display: flex;
  justify-content: space-between;
  flex-direction: row;
`;

const Pagos = () => {
  const [data, setData] = useState([]);
  const [value, setValue] = useState([]);

  useEffect(() => {
    socket.on("res:pagos:view", ({ statusCode, data, message }) => {
      console.log("res:pagos:view", { statusCode, data, message });
      console.log({ statusCode, data, message });

      if (statusCode === 200) {
        setData(data);
      }
    });
    setTimeout(() => socket.emit("req:pagos:view", {}), 1000);
  }, []);
  const handleCreatePago = () => value > 0 ? socket.emit("req:pagos:create", { socio: value, amount: 300 }) : null;
  const handleDeletePago = (id) => socket.emit("req:pagos:delete", { id });
  const handleInput = (e) => setValue(e.target.value);

  return (
    <>
      <Input type={'number'} onChange={handleInput}></Input>
      <Button onClick={handleCreatePago}>Aplicar Pago de Socio</Button>
      <Container>
        {data.map((v, i) => (
          <ContainerSocio key={i}>
            <Body>
              <I>Cupon: {v.id}</I>
              <Name>Socio: {v.socio} </Name>
              <Name>Amount: {v.amount} </Name>
            </Body>
            <Feedback>
              <Name>{v.createdAt} </Name>
              <Icon onClick={() => handleDeletePago(v.id)}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                    clipRule="evenodd"
                  />
                </svg>
              </Icon>
            </Feedback>
          </ContainerSocio>
        ))}
      </Container>
    </>
  );
};

export default Pagos;
<></>;
