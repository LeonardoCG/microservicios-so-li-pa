import React, { useEffect, useState } from "react";
import { socket } from "./ws";
import styled from "styled-components";

const Container = styled.div`
  width: 100%;
  max-width: 700px;
  height: 350px;
  margin-bottom: 15px;
  overflow-y: scroll;
`;
const ContainerSocio = styled.div`
  display: flex;
  flex-direction: column;
  margin-bottom: 10px;
  background-color: #303530;
  border-radius: 20px;
`;
const Body = styled.div`
  padding-left: 15px;
  padding-right: 15px;
  height: 100px;
  text-align: left;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  position: relative;
`;
const Button = styled.button`
  background-color: #039e00;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 10px;
  padding-bottom: 10px;
  border-radius: 15px;
  opacity: 0.8;
  color: white;
  width: 100%;
  height: 50px;
  margin-bottom: 25px;

  cursor: pointer;
`;
const Name = styled.p`
  /* color: #333; */
  color: white;
`;
const Phone = styled.p`
  color: white;
`;
const Email = styled.p`
  color: white;
`;
const Icon = styled.div`
  width: 35px;
  height: 35px;
  color: red;
  opacity: 0.8;
  cursor: pointer;
`;
const I = styled.div`
  position: absolute;
  width: 50px;
  height: 25px;
  top: 8px;
  border-radius: 30%;
  background-color: #b6b6be;
  right: 8px;
  text-align: center;
  padding-top: 5px;
  color: #010101;
  opacity: 0.8;
`;
const Enable = styled.div`
  width: 29px;
  height: 29px;
  border-radius: 50%;
  opacity: 0.8;
  background-color: ${(props) => (props.enable ? "green" : "red")};
  cursor: pointer;
`;
const Feedback = styled.div`
  margin: 10px;
  align-items: center;
  display: flex;
  flex-direction: row-reverse;
`;

const Socios = () => {
  //definimos para setear
  const [data, setData] = useState([
    // {
    //   name: "Leonardo",
    //   phone: "999-999-9999",
    //   email: "company@gmail.com",
    //   enable: true,
    // },
    // {
    //     name: "Leonardo",
    //     phone: "999-999-9999",
    //     email: "company@gmail.com",
    //     enable: true,
    //   },
  ]);

  useEffect(() => {
    //servcio que escucha
    socket.on("res:socios:view", ({ statusCode, data, message }) => {
      console.log("res:socios:view", { statusCode, data, message });
      // datos recibiendo
      console.log({ statusCode, data, message });

      if (statusCode === 200) {
        setData(data);
      }
    });
    //despues de un segundo de escuchar renderizamos
    setTimeout(() => socket.emit("req:socios:view", {}), 1000);
  }, []);

  const handleCreateSocio = () =>
    socket.emit("req:socios:create", {
      name: "Andres",
      phone: "999-999-9999",
    });
  const handleDeleteSocio = (id) => socket.emit("req:socios:delete", { id });
  const handleChange = (id, status) => {
    if (status) socket.emit("req:socios:disable", { id });
    else socket.emit("req:socios:enable", { id });
  };

  return (
    <>
      <Button onClick={handleCreateSocio}>Create Socios</Button>
      <Container>
        {data.map((v, i) => (
          <ContainerSocio key={i}>
            <Body>
              
              <Name>Name: {v.name}</Name> <I>SN: {v.id}</I>
              <Phone> Phone: {v.phone} </Phone>
              <Email>Email: {v.email}</Email>
            </Body>
            <Feedback>
              <Icon onClick={() => handleDeleteSocio(v.id)}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  className="h-5 w-5"
                  viewBox="0 0 20 20"
                  fill="currentColor"
                >
                  <path
                    fillRule="evenodd"
                    d="M10 18a8 8 0 100-16 8 8 0 000 16zM8.707 7.293a1 1 0 00-1.414 1.414L8.586 10l-1.293 1.293a1 1 0 101.414 1.414L10 11.414l1.293 1.293a1 1 0 001.414-1.414L11.414 10l1.293-1.293a1 1 0 00-1.414-1.414L10 8.586 8.707 7.293z"
                    clipRule="evenodd"
                  />
                </svg>
              </Icon>
              <Enable
                enable={v.enable}
                onClick={() => handleChange(v.id, v.enable)}
              />
            </Feedback>
          </ContainerSocio>
        ))}
      </Container>
    </>
  );
};

export default Socios;
