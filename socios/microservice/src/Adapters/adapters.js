// libreria npm i bull
const bull = require('bull');
const { redis, name } = require('../settings');

const opts = { redis: { host: redis.host, port: redis.port } }

const queueCreate = bull(`${name}:create`, opts);

const queueDelete = bull(`${name}:delete`, opts);

const queueUpdate = bull(`${name}:update`, opts);

const queueFindOne = bull(`${name}:findOne`, opts);

const queueView = bull(`${name}:view`, opts);

const queueEnable = bull(`${name}:enable`, opts);

const queueDisable = bull(`${name}:disable`, opts);

module.exports = { 
    queueCreate, 
    queueDelete, 
    queueUpdate, 
    queueFindOne, 
    queueView,
    queueEnable,
    queueDisable 
}


