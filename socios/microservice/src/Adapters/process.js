const { internalError } = require('../settings')
const Services = require('../Services/services');

const { 
    queueCreate, 
    queueDelete, 
    queueUpdate, 
    queueFindOne, 
    queueView, 
    queueEnable,
    queueDisable} = require('./adapters');

//adaptador || interno

const Create = async(job, done) => {

    try {

        const { name, phone } = job.data;

        let { statusCode, data, message }  = await Services.Create({ name, phone });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueCreate', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const Delete = async(job, done) => {

    try {

        const { id } = job.data;

        let { statusCode, data, message }  = await Services.Delete({ id });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueDelete', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const Update = async(job, done) => {

    try {

        const { email, phone, age, id, name } = job.data;

        let { statusCode, data, message }  = await Services.Update({ email, phone, age, id, name });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueDelete', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const FindOne = async(job, done) => {

    try {

        const { id } = job.data;

        let { statusCode, data, message }  = await Services.FindOne({ id });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueFindOne', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const View = async(job, done) => {

    try {

        const {} = job.data;

        console.log(job.id)

        let { statusCode, data, message }  = await Services.View({});

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueView', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const Enable = async(job, done) => {

    try {

        const { id } = job.data;

        let { statusCode, data, message }  = await Services.Enable({ id });

        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueEnable', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const Disable = async(job, done) => {

    try {

        const { id } = job.data;

        let { statusCode, data, message }  = await Services.Disable({ id });
        
        done(null, { statusCode, data, message });
    
    } catch ( error ) {

        console.log({ step:'adapters queueDisable', error: error.toString() });

        done(null, { statusCode: 500, message: internalError });

    }
};

const run = async() => {
    
    try {
        console.log("vamos a inicializar el worker")

        queueCreate.process(Create);

        queueDelete.process(Delete);

        queueUpdate.process(Update);

        queueFindOne.process(FindOne);

        queueView.process(View);

        queueEnable.process(Enable);

        queueDisable.process(Disable);
        
    } catch (error) {
        console.log(error)
        
    }
}
 
module.exports = {
    Create, Delete, Update, FindOne, View, Enable, Disable, run
}