const { Model } = require('../Models/db');


//metodos básicos
const Create = async({ name, phone}) => {
   try {

    let instance = await Model.create(

            { name, phone}, 

            { fields: ['name', 'phone'], logging: false }
        );
        //obtener los datos con .toJSON
        return { statusCode: 200, data: instance.toJSON() }

   } catch (error) {

        console.log({step: 'Controller Create', error: error.toString()});

        return { statusCode: 500, message: error.toString()}

   }
}

const Delete = ({ where = {} }) => {
    try {

        Model.destroy({ where, logging: false });

        //obtener los datos con .toJSON
        return { statusCode: 200, data: "ok" } 
 
    } catch (error) {
         console.log({step: 'Controller Delete', error: error.toString()});
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

const Update = async({ email, phone, age, id, name }) => {
    try {
     
        let instance = await Model.update(
            { email, phone, age, id, name }, 
            { where: { id }, logging: false, returning: true } //devuelve los elementos instancedos returning: true 
        );
        //obtener los datos con .toJSON obteniedo la unica instancia actualizada
        return { statusCode: 200, data: instance[1][0].toJSON() }

    } catch (error) {
         console.log({ step: 'Controller Update', error: error.toString() });
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

const FindOne = async({ where = {} }) => {
    
    try {
     
        let instance = await Model.findOne({ where, logging: false });

        //obtener los datos con .toJSON 
        if( instance ) return { statusCode: 200, data: instance.toJSON() }

        else return {statusCode: 400, message: "No existe el usuario"}
 
    } catch (error) {
         console.log({step: 'Controller FindOne', error: error.toString()});
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

const View = async({ where = {} }) => {
    try {

        let instances = await Model.findAll({ where, logging: false });

        //obtener los datos con .toJSON
        return { statusCode: 200, data: instances }


    } catch (error) {
         console.log({step: 'Controller View', error: error.toString()});
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

const Enable = async({ where: {id }}) => {

    try {

        let instance = await Model.update(
            { enable: true }, 
            { where: { id }, logging: false, returning: true});
        //obtener los datos con .toJson
        return { statusCode: 200, data: instance[1][0].toJSON() }

    } catch (error) {
        
        console.log({ step: 'Controller Enable', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }
}

const Disable = async({ where: {id }}) => {

    try {

        let instace = await Model.update(
            { enable: false }, 
            { where: { id }, logging: false, returning: true });
        //obtener datos con .toJson
        return { statusCode: 200, data: instace[1][0].toJSON() }
        
    } catch (error) {
        
        console.log({ step: 'Controller Disable', error: error.toString()});

        return { statusCode: 400, message: "No existe el usuario"}
    }
}

module.exports = {
    Create, Delete, Update, FindOne, View, Enable, Disable
}