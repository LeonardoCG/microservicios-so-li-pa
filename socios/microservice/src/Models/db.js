const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');


//modelo 
const Model = sequelize.define('socio', {

    name: { 
        type: DataTypes.STRING 
    },
    age: {     
        type: DataTypes.BIGINT 
    },
    email: {
        type: DataTypes.STRING 
    },
    phone: { 
        type: DataTypes.STRING 
    },
    //socios activos
    enable: {
        type: DataTypes.BOOLEAN,
        defaultValue: true
    },

}, { freezeTableName: true }); //para que no se modifique el nombre de la table

//iniciarlizar el model 
const syncroDB = async() => {

    try {
        
        // detnemos el registro de logueo y force borra la DB{ logging: false,  force: true }
        await Model.sync({ logging: false})

        console.log('base de datos iniciada - Socios')

        return { statusCode: 200, data: 'ok'}
 
    } catch (error) {
         console.log( error );
 
         return { statusCode: 500, message: error.toString()}
 
    }
}


module.exports = { Model, syncroDB };