const Controllers = require('../Controllers/controllers');
const { internalError } = require('../settings');

//Servicios  || exterior
const Create = async({ phone, name }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.Create({ phone, name});

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service Create', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const Delete = async({ id }) => {

    try {

        const findOne = await Controllers.FindOne({ where: { id }});

        if( findOne.statusCode !== 200 ) {

            switch( findOne.statusCode ) {

                case 400: return { statusCode: 400, message: "No existe el usuario eliminado" }

                default: return { statusCode: 500, message: internalError }
            }
        }

        const del = await Controllers.Delete({ where: { id } });

        if( del.statusCode === 200 ) return { statusCode: 200, data: findOne.data }

        return { statusCode: 400, message: internalError }

    } catch (error) {
       
        console.log({ step: 'service Delete', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const Update = async({ email, phone, age, id, name }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.Update({ email, phone, age, id, name });

        return { statusCode, data, message }

    } catch (error) {
       
        console.log({ step: 'service Update', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const FindOne = async({ id }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.FindOne({ where: { id }});

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service FindOne', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const View = async({ enable }) => {

    try {

        let where = {}
        if( enable !== undefined ) where.enable = enable
        
        let { statusCode, data, message } = await Controllers.View({ where });

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service View', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const Enable = async({ id }) => {
    
    try {

        let { statusCode, data, message } = await Controllers.Enable({ where: { id }});

        return { statusCode, data, message }
        
    } catch (error) {
        
        console.log({ step: 'service Enable', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }
}

const Disable = async({ id }) => {
    
    try {
        
        let { statusCode, data, message } = await Controllers.Disable({ where: { id }});
      
        return { statusCode, data, message }
        
    } catch (error) {
        
        console.log({ step: 'service Disable', error: error.toString() })
        
        return { statusCode: 500, message: error.toString() }
    }
}

module.exports = { 
    Create, Delete, Update, FindOne, View, Enable, Disable 
}
