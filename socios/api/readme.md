## API DE SOCIOS

esta es una api para interactuar con los SOCIOS registrados 

'''js
    const apiSocios = require('api-socios');

      socket.on('req:socios:view', async({ enable }) => {

            try {

                console.log('req:socios:view');
                
                const { statusCode, data, message } = await apiSocios.View({ enable });
                //emitimos la respuesta al socked.id que hizo la consulta
                return io.to(socket.id).emit('res:socios:view', { statusCode, data, message });
                
            } catch (error) {
                console.log(error)
                
            }
            
        });