// libreria npm i bull
const bull = require('bull');

const {name } = require('./package.json');

const redis = { host: '192.168.1.105' , port: 6379 }

const opts = { redis: { host: redis.host, port: redis.port } }

const queueCreate = bull(`${name.replace('api-', '')}:create`, opts);

const queueDelete = bull(`${name.replace('api-', '')}:delete`, opts);

const queueUpdate = bull(`${name.replace('api-', '')}:update`, opts);

const queueFindOne = bull(`${name.replace('api-', '')}:findOne`, opts);

const queueView = bull(`${name.replace('api-', '')}:view`, opts);

const queueEnable = bull(`${name.replace('api-', '')}:enable`, opts);

const queueDisable = bull(`${name.replace('api-', '')}:disable`, opts);


const Create = async({ name, phone }) => {

    try {
        const job = await queueCreate.add({ name, phone });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const Delete = async({ id }) => {

    try {
        const job = await queueDelete.add({ id });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }
        

    } catch (error) {
        console.log(error)
    }

}

const Update = async({ email, phone, age, id, name }) => {

    try {
        const job = await queueUpdate.add({ email, phone, age, id, name });

        const { statusCode, data, message } = await job.finished(); 

       return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const FindOne = async({ id }) => {

    try {
        const job = await queueFindOne.add({ id });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const View = async({ enable }) => {

    try {

        const job = await queueView.add({ enable });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const Enable = async({ id }) => {

    try {
        const job = await queueEnable.add({ id });

        const { statusCode, data, message } = await job.finished(); 

       return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const Disable = async({ id }) => {

    try {
        const job = await queueDisable.add({ id });

        const { statusCode, data, message } = await job.finished(); 

       return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

module.exports =  { Create, Delete, Update, FindOne, Enable, Disable, View }