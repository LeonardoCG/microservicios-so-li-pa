// libreria npm i bull
const bull = require('bull');

const { name } = require('../../package.json');

const { redis} = require('../settings');

const opts = { redis: { host: redis.host, port: redis.port } }

const queueCreate = bull(`${name}:create`, opts);

const queueDelete = bull(`${name}:delete`, opts);

const queueUpdate = bull(`${name}:update`, opts);

const queueFindOne = bull(`${name}:findOne`, opts);

const queueView = bull(`${name}:view`, opts);

module.exports = { 
    queueCreate, 
    queueDelete, 
    queueUpdate, 
    queueFindOne, 
    queueView 
}


