const { Model } = require('../Models/db');


//metodos básicos
const Create = async({ title, image }) => {
   try {

    let instance = await Model.create(
            { title, image }, 
            { fields: ['title', 'image'], logging: false } //fields se usa cuando son muchos campos
        );

        //obtener los datos con .toJSON
        return { statusCode: 200, data: instance.toJSON() }


   } catch (error) {
        console.log({step: 'Controller Create', error: error.toString()});

        return { statusCode: 500, message: error.toString()}

   }
}

const Delete = ({ where = {} }) => {
    try {

        Model.destroy({ where, logging: false });

        //obtener los datos con .toJSON
        return { statusCode: 200, data: "ok" } 
 
    } catch (error) {
         console.log({step: 'Controller Delete', error: error.toString()});
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

const Update = async({ title, image, category, seccions, id }) => {
    try {
     
        let instance = await Model.update(
            { title, image, category, seccions, id}, 
            { where: { id }, logging: false, returning: true } //devuelve los elementos instancedos returning: true 
        );
        //obtener los datos con .toJSON obteniedo la unica instancia actualizada
        return { statusCode: 200, data: instance[1][0].toJSON() }

    } catch (error) {
         console.log({ step: 'Controller Update', error: error.toString() });
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

const FindOne = async({ where = {} }) => {
    try {
     
        let instance = await Model.findOne({ where, logging: false });

        //obtener los datos con .toJSON 
        if( instance ) return { statusCode: 200, data: instance.toJSON() }

        else return {statusCode: 400, message: "No existe el libro"}
 
    } catch (error) {
         console.log({step: 'Controller FindOne', error: error.toString()});
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

const View = async({ where = {} }) => {
    try {

        let instances = await Model.findAll({ where , logging: false });

        //obtener los datos con .toJSON
        return { statusCode: 200, data: instances }


    } catch (error) {
         console.log({step: 'Controller View', error: error.toString()});
 
         return { statusCode: 500, message: error.toString()}
 
    }
 }

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View
}