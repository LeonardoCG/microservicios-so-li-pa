const { sequelize } = require('../settings');
const { DataTypes } = require('sequelize');


//modelo 
const Model = sequelize.define('libro', {

    title: { 
        
        type: DataTypes.STRING 
    },
    category: { 
        
        type: DataTypes.BIGINT 
    },
    seccions: { 
        
        type: DataTypes.STRING 
    },
    image: {
        type: DataTypes.STRING
    }

});

//iniciarlizar el model 
const syncroDB = async () => {

    try {

        // detnemos el registro de logueo { logging: false }
        await Model.sync({ logging: false })

        console.log('base de datos iniciada - Libros')

        return { statusCode: 200, data: 'ok' }

    } catch (error) {
        console.log(error);

        return { statusCode: 500, message: error.toString() }

    }
}


module.exports = { Model, syncroDB };