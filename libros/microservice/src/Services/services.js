const Controllers = require('../Controllers/controllers');
const { internalError } = require('../settings');

//Servicios  || exterior
const Create = async({ title, image }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.Create({ title, image });

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service Create', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const Delete = async({ id }) => {

    try {

        const findOne = await Controllers.FindOne({ where: { id }});

        if( findOne.statusCode !== 200 ) {

            switch( findOne.statusCode ) {
                case 400: return { statusCode: 400, message: "No existe el libro eliminado" }

                default:  return { statusCode: 500, message: internalError }
            }
        }

        const del = await Controllers.Delete({ where: { id } });

        if( del.statusCode === 200 ) return { statusCode: 200, data: findOne.data }

        return { statusCode: 400, message: internalError }

    } catch (error) {
       
        console.log({ step: 'service Delete', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const Update = async({ title, image, category, seccions, id }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.Update({ title, image,category, seccions, id });

        return { statusCode, data, message }

    } catch (error) {
       
        console.log({ step: 'service Update', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const FindOne = async({ title }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.FindOne({ where: { title }});

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service FindOne', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

const View = async({ }) => {

    try {
        
        let { statusCode, data, message } = await Controllers.View({ where: { }});

        return { statusCode, data, message}

    } catch (error) {
       
        console.log({ step: 'service View', error: error.toString() });

        return { statusCode: 500, message: error.toString() }
    }

}

module.exports = {
    Create,
    Delete,
    Update,
    FindOne,
    View
}
