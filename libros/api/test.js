// libreria npm i bull
const bull = require('bull');

const { name } = require('./package.json');

const redis = { host: '192.168.1.105' , port: 6379 }

const opts = { redis: { host: redis.host, port: redis.port } }

const queueCreate = bull(`${name.replace('api-', '')}:create`, opts);

const queueDelete = bull(`${name.replace('api-', '')}:delete`, opts);

const queueUpdate = bull(`${name.replace('api-', '')}:update`, opts);

const queueFindOne = bull(`${name.replace('api-', '')}:findOne`, opts);

const queueView = bull(`${name.replace('api-', '')}:view`, opts);

const Create = async({ title, image }) => {

    try {
        const job = await queueCreate.add({ title, image });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }
}

const Delete = async({ id }) => {

    try {
        const job = await queueDelete.add({ id });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }
        

    } catch (error) {
        console.log(error)
    }

}

const Update = async({ title, image, category, seccions, id }) => {

    try {
        const job = await queueUpdate.add({ title, image, category, seccions, id });

        const { statusCode, data, message } = await job.finished(); 

       return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const FindOne = async({ title }) => {

    try {
        const job = await queueFindOne.add({ title });

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

const View = async({ }) => {

    try {

        const job = await queueView.add({});

        const { statusCode, data, message } = await job.finished(); 

        return { statusCode, data, message }

    } catch (error) {
        console.log(error)
    }

}

module.exports =  { Create, Delete, Update, FindOne, View }